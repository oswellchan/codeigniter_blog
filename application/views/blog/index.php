<?php
if (empty($user)) { ?>
    <p><a href="blog/login">Login</a> <a href="blog/register">Register</a></p>
<?php 
} else { ?>
    <p>Welcome, <?php echo $user.'.' ?> </p> <p><a href="blog/create">Create a blog post</a> <a href="blog/logout">Logout</a></p>
<?php }

if (count($posts) === 0) { ?>
    <p>There are no posts at the moment.</p>
<?php 
} else {

    foreach ($posts as $post): ?>
        <h2><?php echo $post['title'] ?></h2>
        <div class="main">
            <?php echo $post['text'] ?>
        </div>
        <p>Author: <?php echo $post['author'] ?> </p>
        <p><?php if (!empty($user)) { 
                if ($this->session->userdata('username') === $post['author']) { ?>
                    <a href="blog/edit/<?php echo $post['slug'] ?>">Edit</a>
                    <a href="blog/delete/<?php echo $post['slug'] ?>">Delete</a>
            <?php }
            } ?>
        <a href="blog/<?php echo $post['slug'] ?>">View article</a></p>


<?php endforeach; } ?>