<h2>Enter your credentials below</h2>

<?php if ($login === 'failed') { ?>
    <p>Credentials are wrong. Please enter again.</p>
<?php } ?>

<?php echo validation_errors(); ?>

<?php echo form_open('blog/login') ?>

	<label for="username">Username</label>
	<input type="input" name="username" /><br />

	<label for="password">Password</label>
	<input type="password" name="password" /><br />

	<input type="submit" name="submit" value="Submit" />

</form>