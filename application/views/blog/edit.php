<h2>Edit blog post</h2>

<?php echo validation_errors(); ?>

<?php echo form_open('blog/edit/'.$post['slug']) ?>

	<label for="title">Title</label>
	<input type="input" name="title" value="<?php echo $post['title']; ?>"/><br />

	<label for="text">Text</label>
	<textarea name="text"><?php echo $post['text']; ?></textarea><br />

	<input type="submit" name="submit" value="Save changes" />

</form>