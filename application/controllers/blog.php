<?php
class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
        $this->load->library('session');
	}

	public function index()
	{
        $user = $this->session->userdata('username');
        
		$data['posts'] = $this->blog_model->get_posts();
        $data['title'] = 'Blog Posts';
        $data['user'] = $user;

	   $this->load->view('templates/header', $data);
	   $this->load->view('blog/index', $data);
	   $this->load->view('templates/footer');
	}
    
    public function home()
	{
        $this->load->helper('url');
        redirect('/', 'refresh');
	}
    
    public function login()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Login';
        $data['login'] = 'notyet';
        
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('blog/login', $data);
            $this->load->view('templates/footer');

        }
        else
        {
            $result = $this->blog_model->validate_login();
            
            if (count($result) !== 0) {
                $this->session->set_userdata(array('username' => $result['user']));
                $this->load->helper('url');
                redirect('/', 'refresh');
            } else {
                $data['login'] = 'failed';
                $this->load->view('templates/header', $data);
                $this->load->view('blog/login', $data);
                $this->load->view('templates/footer');
            }
	   }
    }
    
    public function register()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Register';
        $data['errormsg'] = '';
        
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('blog/register', $data);
            $this->load->view('templates/footer');

        }
        else
        {
            $isSuccessful = $this->blog_model->validate_register();
            
            if ($isSuccessful) {
                $this->session->set_userdata(array('username' => $this->input->post('username')));
                $this->load->helper('url');
                redirect('/', 'refresh');
            } else {
                $data['errormsg'] = 'Username already exists. Please choose another.';
                $this->load->view('templates/header', $data);
                $this->load->view('blog/register', $data);
                $this->load->view('templates/footer');
            }
	   }
    }
    
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->load->helper('url');
        redirect('/', 'refresh');
    }

	public function view($slug)
	{
		$data['post'] = $this->blog_model->get_posts($slug);

	   if (empty($data['post']))
	   {
		  show_404();
	   }

        $data['title'] = $data['post']['title'];

        $this->load->view('templates/header', $data);
        $this->load->view('blog/view', $data);
        $this->load->view('templates/footer');
	}
    
    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a blog post';

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('text', 'text', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('blog/create');
            $this->load->view('templates/footer');

        }
        else
        {
            $slug = $this->blog_model->set_post();
            $data['slug'] = $slug;
            $this->load->view('templates/header', $data);
            $this->load->view('blog/success', $data);
            $this->load->view('templates/footer');
	   }
    }
    
    public function delete($slug)
    {
        $data['title'] = 'Delete Post';
        $data['delete'] = false;
        
        $data['post'] = $this->blog_model->get_posts($slug);
        
        if (!empty($data['post']) && $data['post']['author'] === $this->session->userdata('username')) {
            $this->load->view('templates/header', $data);
            $this->load->view('blog/delete', $data);
            $this->load->view('templates/footer');
        } else {
            $this->load->helper('url');
            redirect('/', 'refresh');
        }
    }
    
    public function delete_yes($slug)
    {
        $this->blog_model->delete_post($slug);
        $data['title'] = 'Post deleted';
        $data['delete'] = true;
        
        $this->load->view('templates/header', $data);
        $this->load->view('blog/delete', $data);
        $this->load->view('templates/footer');
    }
    
    public function delete_no()
    {
        $this->load->helper('url');
        redirect('/', 'refresh');
    }
    
    public function edit($slug)
    {
        $data['title'] = 'Edit Post';
        
        $data['post'] = $this->blog_model->get_posts($slug);
        
        if (!empty($data['post']) && $data['post']['author'] === $this->session->userdata('username')) {
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('text', 'text', 'required');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('blog/edit', $data);
                $this->load->view('templates/footer');

            }
            else
            {
                $slug = $this->blog_model->edit_post($data['post']['id']);
                $data['slug'] = $slug;
                $this->load->view('templates/header', $data);
                $this->load->view('blog/success_edited', $data);
                $this->load->view('templates/footer');
            }
        } else {
            $this->load->helper('url');
            redirect('/', 'refresh');
        }
    }
}