<?php
class Blog_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
    
    public function get_posts($slug = FALSE)
    {
        if ($slug === FALSE)
        {
            $query = $this->db->get('posts');
            return $query->result_array();
        }

        $query = $this->db->get_where('posts', array('slug' => $slug));
        return $query->row_array();
    }
    
    public function edit_post($id)
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'), 'dash', TRUE);

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'text' => $this->input->post('text'),
            'author' => $this->session->userdata('username')
        );

        $this->db->where('id', $id);
        $this->db->update('posts', $data);
        
        return $slug;
    }
    
    public function delete_post($slug = FALSE)
    {
        return $this->db->delete('posts', array('slug' => $slug));
    }
    
    public function set_post()
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'), 'dash', TRUE);

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'text' => $this->input->post('text'),
            'author' => $this->session->userdata('username')
        );

        $this->db->insert('posts', $data);
        
        return $slug;
    }
    
    public function validate_login()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $query = $this->db->get_where('userData', array('user' => $user, 'password' => $pass));
        
        return ($query->row_array());
    }
    
    public function validate_register()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $query = $this->db->get_where('userData', array('user' => $user));
        
        if (empty($query->row_array())) {
            $data = array(
                'user' => $user,
                'password' => $pass
            );
            
            $this->db->insert('userdata', $data);
            
            return true;
        }
        
        return false;
    }
}